//
//  SCStartModel.swift
//  SwiftClient
//
//  Created by Dabao on 14/12/1.
//  Copyright (c) 2014年 Dabao. All rights reserved.
//

import UIKit

class SCStartModel: NSObject {
    var id: Int!
    var name: NSString!
    var year: NSString!
}
