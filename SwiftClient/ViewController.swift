//
//  ViewController.swift
//  SwiftClient
//
//  Created by Dabao on 14/12/1.
//  Copyright (c) 2014年 Dabao. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var scTableView: UITableView?
    var names: NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "title"
        
        var rightBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: "refresh:")
        self.navigationItem.rightBarButtonItem = rightBarItem
        
        var leftBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "add")
        self.navigationItem.leftBarButtonItem = leftBarItem
        
        // Do any additional setup after loading the view, typically from a nib.
        scTableView = UITableView(frame: self.view.bounds, style: UITableViewStyle.Plain)
        scTableView?.dataSource = self;
        scTableView?.delegate = self;
        self.view.addSubview(scTableView!)
    
        self.names = NSMutableArray(capacity: 10)
        
        
        for var i = 0; i < 100; i++ {
            let startModel = SCStartModel()
            startModel.name = "dabao \(i)"
            startModel.year = "2014年"
            startModel.id = i
            self.names.addObject(startModel)
        }
    }
    
    func refresh(sender: UIBarButtonItem) {
        self.scTableView?.reloadData()
    }
    
    func add() {
        
        
        var alertViewController = UIAlertController(title: "添加", message: "标题", preferredStyle: UIAlertControllerStyle.Alert)
        
        var cancelAction = UIAlertAction(title: "取消", style: UIAlertActionStyle.Destructive) { (cancelAction) -> Void in
            // ...
        }
        
        alertViewController.addAction(cancelAction)
        
        let sureAction = UIAlertAction(title: "确定", style: UIAlertActionStyle.Default) { (sureAction) -> Void in
            // ..
            let titleTextField = alertViewController.textFields![0] as UITextField
            let start = SCStartModel()
            start.name = titleTextField.text;
            self.names.insertObject(start, atIndex: 0)
            self.scTableView?.reloadData()

        }
        
        alertViewController.addAction(sureAction)

        alertViewController.addTextFieldWithConfigurationHandler { (titleTextField) -> Void in
            titleTextField.placeholder = "dabao"
        }
        
        self.presentViewController(alertViewController, animated: true) { () -> Void in
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.names.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "SCCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: cellIdentifier)
        }
        
        var start: SCStartModel = self.names[indexPath.row] as SCStartModel
        cell?.textLabel.text = start.name
        cell?.detailTextLabel?.text = start.year;
        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var view = ViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 120
    }
    
}

